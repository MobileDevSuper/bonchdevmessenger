package bonch.dev.school.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Message(val msgId:Int, val msgText: String?, val sentDate: String?, val isUser:Boolean):
    Parcelable {

    class MessageLab{
        val msgList:MutableList<Message> = mutableListOf()
        private val formatDate = SimpleDateFormat("hh:mm:ss")

        init {
            for (i in 1..20){
                val userMessage = Message(i,"Сообщение №$i",formatDate.format(Date()),false)
                msgList.add(userMessage)
            }
        }
    }

}