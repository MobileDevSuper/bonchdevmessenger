package bonch.dev.school.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import bonch.dev.school.R
import bonch.dev.school.models.Message
import java.text.SimpleDateFormat
import java.util.*

class MessageRecyclerItems(val messageList:MutableList<Message>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val myMsg=1
    private val otherMsg=2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            myMsg-> myViewHolder((LayoutInflater.from(parent.context).inflate(R.layout.user_message_item,parent,false)))
            otherMsg-> OtherViewHolder((LayoutInflater.from(parent.context).inflate(R.layout.other_message_item,parent,false)))
            else -> myViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_message_item,parent,false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when(getItemViewType(position)){
            myMsg-> (holder as myViewHolder).bind(position)
            otherMsg->(holder as OtherViewHolder).bind(position)
        }
    }
    
    override fun getItemCount(): Int {
        return messageList.size
    }
    
    inner class OtherViewHolder(v:View):RecyclerView.ViewHolder(v){
        private val otherMsgText:TextView=v.findViewById(R.id.other_message_text)
        private val otherDateText:TextView=v.findViewById(R.id.other_message_date)
        private val otherNameText:TextView=v.findViewById(R.id.other_message_sender_name)
        private val formatDate = SimpleDateFormat("hh:mm:ss")
        
        fun bind(position: Int){
            otherDateText.text=formatDate.format(Date())
            otherMsgText.text="${messageList[position].msgText}"
            otherNameText.text="SomeGuy"
        }
    }
    inner class myViewHolder(v:View):RecyclerView.ViewHolder(v){
        private val showText:TextView =v.findViewById(R.id.show_message_text)
        private val showDate:TextView=v.findViewById(R.id.sent_message_date)
        private val formatDate = SimpleDateFormat("hh:mm:ss")

        fun bind(position: Int){
            showText.text="${messageList[position].msgText}"
            showDate.text=formatDate.format(Date())
        }

    }
    override fun getItemViewType(position: Int): Int {
        return if (messageList[position].isUser){
            myMsg
        } else otherMsg
    }

}

