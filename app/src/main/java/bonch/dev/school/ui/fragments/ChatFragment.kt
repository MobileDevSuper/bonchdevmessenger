package bonch.dev.school.ui.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bonch.dev.school.R
import bonch.dev.school.models.Message
import bonch.dev.school.ui.MessageRecyclerItems
import kotlinx.android.synthetic.main.fragment_chat.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ChatFragment : Fragment() {

    private lateinit var adapter: MessageRecyclerItems
    private lateinit var lm: LinearLayoutManager
    private lateinit var msgRecycler: RecyclerView
    private lateinit var divider: LinearLayout
    private var recyclerViewStateBundle: Bundle = Bundle()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        items = mBundleRecyclerViewState?.getParcelableArrayList<Message>("CHATS") ?: items
        adapter = MessageRecyclerItems(items)
        Log.e("FragmentOnCView", "${items}")

        val view = inflater.inflate(R.layout.fragment_chat, container, false)

        val sendButton: ImageButton = view.findViewById(R.id.send_message_button)
        val msgEdit: EditText = view.findViewById(R.id.message_et)
        msgRecycler = view.findViewById(R.id.message_recycler_view)
        divider = view.findViewById(R.id.divider2)
        lm = LinearLayoutManager(container!!.context)
        msgRecycler.layoutManager = lm
        msgRecycler.adapter = MessageRecyclerItems(items)
        (msgRecycler.scrollToPosition((msgRecycler.adapter as MessageRecyclerItems).messageList.size - 1))
        msgEdit.setOnFocusChangeListener { view, hasFocus ->
            if (msgEdit.hasFocus()) {
                divider.setBackgroundResource(R.drawable.divider_active)

            } else {
                divider.setBackgroundResource(R.drawable.divider_chat)

            }
        }

        sendButton.setOnClickListener {

            if (msgEdit.text.isEmpty()) {
                Toast.makeText(context, "Пустая строка", Toast.LENGTH_SHORT).show()

            } else {
                val formatDate = SimpleDateFormat("hh:mm:ss")
                items.add(Message(1, msgEdit.text.toString(), formatDate.format(Date()), true))
                (msgRecycler.scrollToPosition((msgRecycler.adapter as MessageRecyclerItems).messageList.size - 1))
                msgEdit.setText("")

            }
        }

        return view
    }

    override fun onPause() {
        super.onPause()

        mBundleRecyclerViewState = Bundle()
        mBundleRecyclerViewState?.putParcelableArrayList("CHATS", ArrayList<Parcelable>(items))
    }

    override fun onResume() {
        super.onResume()

        if (mBundleRecyclerViewState != null) {
            items = mBundleRecyclerViewState?.getParcelableArrayList<Message>("CHATS")!!
            adapter.notifyItemInserted(items.size - 1)
            message_recycler_view.scrollToPosition(items.size - 1)
        }
    }

    companion object {

        private var mBundleRecyclerViewState: Bundle? = null

        @JvmStatic
        fun newInstance() = ChatFragment()
    }

    var items = arrayListOf(
        Message(0, "SimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(1, "SimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false),
        Message(2, "SimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(3, "SimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false),
        Message(4, "SimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(5, "SimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(6, "SimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false),
        Message(7, "SimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(8, "SimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false),
        Message(9, "ffff", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(10, "SimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false),
        Message(11, "SimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), true),
        Message(12, "SimpleTextSimpleText", SimpleDateFormat("hh:mm:ss").format(Date()), false)
    )


}